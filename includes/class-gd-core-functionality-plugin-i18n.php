<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.greendesk.pl
 * @since      1.0.0
 *
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 * @author     Przemek Cichon <przemek@greendesk.pl>
 */
class Gd_Core_Functionality_Plugin_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'gd-core-functionality-plugin',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}

    public function acf_settings_localization($localization){
        return true;
    }


    public function acf_settings_textdomain($domain){
        return 'gd-core-functionality-plugin';
    }



}
