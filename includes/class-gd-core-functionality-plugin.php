<?php



/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.greendesk.pl
 * @since      1.0.0
 *
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 * @author     Przemek Cichon <przemek@greendesk.pl>
 */

class Gd_Core_Functionality_Plugin {
	
	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Gd_Core_Functionality_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */

	protected $loader;
	
	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	
	protected $plugin_name;
	
	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	
	protected $version;	
	
	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	
	public function __construct() {

		$this->plugin_name = 'gd-core-functionality-plugin';		
		$this->version = '1.1.0';		
		$this->load_dependencies();
		$this->set_locale();	
		$this->define_admin_hooks();
		$this->define_public_hooks();	
		$this->define_acf_fields();

	}
	
	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Gd_Core_Functionality_Plugin_Loader. Orchestrates the hooks of the plugin.
	 * - Gd_Core_Functionality_Plugin_i18n. Defines internationalization functionality.
	 * - Gd_Core_Functionality_Plugin_Admin. Defines all hooks for the admin area.
	 * - Gd_Core_Functionality_Plugin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	
	private function load_dependencies() {
	
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gd-core-functionality-plugin-loader.php';
		
		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gd-core-functionality-plugin-i18n.php';
		
		/**
         * The class responsible for creating
         * custom post types
         */	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gd-core-functionality-plugin-custom-post-type.php';
			
		/**
		 * The class responsible for creating
		 * acf fields
		 */	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gd-core-functionality-plugin-acf.php';	
		
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions/custom-logo.php';
		
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-gd-core-functionality-plugin-admin.php';
	
		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-gd-core-functionality-plugin-public.php';
	
		$this->loader = new Gd_Core_Functionality_Plugin_Loader();
	
	}
	
	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Gd_Core_Functionality_Plugin_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
	
		$plugin_i18n = new Gd_Core_Functionality_Plugin_i18n();		
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );		
		$this->loader->add_filter('acf/settings/l10n', $plugin_i18n, 'acf_settings_localization' );		
		$this->loader->add_filter('acf/settings/l10n_textdomain', $plugin_i18n, 'acf_settings_textdomain' );
		
	}
	
	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Gd_Core_Functionality_Plugin_Admin( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );		
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_filter( 'brunch_pro_get_google_fonts', $plugin_admin, 'get_google_fonts' );
		
	}
	
	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */	
	private function define_public_hooks() {	
		
		$plugin_public = new Gd_Core_Functionality_Plugin_Public( $this->get_plugin_name(), $this->get_version() );	
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	
	}
	
	/**
	 * Register all of the hooks related to the acf fields
	 * creation.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_acf_fields() {
		
		$plugin_acf = new Gd_Core_Functionality_Plugin_Acf();
		$this->loader->add_filter( 'acf/settings/save_json', $plugin_acf, 'acf_json_save_point' );
		$this->loader->add_filter( 'acf/settings/load_json', $plugin_acf, 'acf_json_load_point' );
		$this->loader->add_filter( 'acf/update_value/name=cfp_item_details_related_items_table', $plugin_acf, 'bidirectional_acf_update_value', 10, 3 );
		
	}	
	
	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		
		$this->loader->run();
		
	}
	
	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		
		return $this->plugin_name;
		
	}
	
	
	
	
	
	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Gd_Core_Functionality_Plugin_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		
		return $this->loader;
		
	}
	
	
	
	
	
	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		
		return $this->version;
		
	}

}


