<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.greendesk.pl
 * @since      1.0.0
 *
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 * @author     Przemek Cichon <przemek@greendesk.pl>
 */
class Gd_Core_Functionality_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
