<?php

/**
 * Define the acf fields
 *
 * Registers acf fields
 *
 * @link       http://www.greendesk.pl
 * @since      1.0.0
 *
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 */

/**
 * Define the acf fields
 *
 * Registers acf fields
 *
 * @since      1.0.0
 * @package    Gd_Core_Functionality_Plugin
 * @subpackage Gd_Core_Functionality_Plugin/includes
 * @author     Przemek Cichon <przemek@greendesk.pl>
 */
class Gd_Core_Functionality_Plugin_Acf{

    public function __construct() {
        
    }

     /**
      * acf_json_save_point
      *
      * Sets the absolute path to folder where json files will be created when field groups are saved.
      *
      * @since    1.0.0
      * @see plugin_dir_path()
      * @param string $path
      * @return path
     */
    public function acf_json_save_point ( $path ) {

        // update path
        $path = plugin_dir_path( __FILE__ ) . 'json';
        return $path;
    }

    /**
     * acf_json_load_point
     *
     * Sets array of absolutes paths to folders where field group json files can be read.
     *
     * @since    1.0.0
     * @see plugin_dir_path()
     * @param string $paths
     * @return paths
     */
    public function acf_json_load_point ( $paths ) {
        
        // append path
        $paths[] = plugin_dir_path( __FILE__ ) . 'json';
        return $paths;
    }

    /**
     * bidirectional_acf_update_value
     *
     * Saves bidirection relationship between posts of the same type.
     *
     * @since    1.0.1
     * @param string $value
     * @param int $post_id
     * @param string $field
     * @TODO make it to work from this plugin, temporarily moving it to functions.php
     * @link https://www.advancedcustomfields.com/resources/bidirectional-relationships/
     * @return value
     */
    public function bidirectional_acf_update_value( $value, $post_id, $field  ) {

        // vars
        $field_name = $field['name'];
        $global_name = 'is_updating_' . $field_name;


        // bail early if this filter was triggered from the update_field() function called within the loop below
        // - this prevents an inifinte loop
        if( !empty($GLOBALS[ $global_name ]) ) return $value;


        // set global variable to avoid inifite loop
        // - could also remove_filter() then add_filter() again, but this is simpler
        $GLOBALS[ $global_name ] = 1;


        // loop over selected posts and add this $post_id
        if( is_array($value) ) {

            foreach( $value as $post_id2 ) {

                // load existing related posts
                $value2 = get_field($field_name, $post_id2, false);


                // allow for selected posts to not contain a value
                if( empty($value2) ) {

                    $value2 = array();

                }


                // bail early if the current $post_id is already found in selected post's $value2
                if( in_array($post_id, $value2) ) continue;


                // append the current $post_id to the selected post's 'related_posts' value
                $value2[] = $post_id;


                // update the selected post's value
                update_field($field_name, $value2, $post_id2);

            }

        }


        // find posts which have been removed
        $old_value = get_field($field_name, $post_id, false);

        if( is_array($old_value) ) {

            foreach( $old_value as $post_id2 ) {

                // bail early if this value has not been removed
                if( is_array($value) && in_array($post_id2, $value) ) continue;


                // load existing related posts
                $value2 = get_field($field_name, $post_id2, false);


                // bail early if no value
                if( empty($value2) ) continue;


                // find the position of $post_id within $value2 so we can remove it
                $pos = array_search($post_id, $value2);


                // remove
                unset( $value2[ $pos] );


                // update the un-selected post's value
                update_field($field_name, $value2, $post_id2);

            }

        }


        // reset global varibale to allow this filter to function as per normal
        $GLOBALS[ $global_name ] = 0;


        // return
        return $value;

    }
        
}